@extends('layouts.app')

@section('content')
<div class="container">
    <div class="border-bottom pb-3 mb-3">
        <h3 class="pt-5 border-bottom pb-1 mb-3 ">User: {{$user->name}}</h3>
        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 .img-fluid. w-100%  h-auto ">
            <div class="col mb-4">
                <h4>Information about user:</h4>
                <p>email: {{$user->email}}</p>
                <p>rating: {{($user->posts->count() + $rating_quality + $rating_actuality) / 3}}</p>
            </div>
        </div>
    </div>
    </div>
@endsection
