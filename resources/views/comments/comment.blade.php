<div class="container">
    <div class="media g-mb-30 media-comment">
        <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
            <div class="g-mb-15">
                @can('delete', $comment)
                    <form method="post" action="{{route('comments.destroy', ['comment' => $comment])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                @endcan
                <h4 class="h5 g-color-gray-dark-v1 mb-0">Satisfied:{{$comment->satisfied}}</h4>
                    <h4 class="h5 g-color-gray-dark-v1 mb-0">Quality:{{$comment->quality}}</h4>
                    <h4 class="h5 g-color-gray-dark-v1 mb-0">Actuality:{{$comment->actuality}}</h4>
                    <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
                <span class="g-color-gray-dark-v4 g-font-size-12">{{$comment->created_at->diffForHumans()}}</span>
            </div>
            <p>
                {{$comment->body}}
            </p>
        </div>
    </div>
</div>
