@extends('layouts.app')
@section('content')
    <div class="container">
    <h1>Edit post</h1>
    <form enctype="multipart/form-data" method="post" action="{{ route('posts.update', ['post' => $post]) }}">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="body">Body</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body"
                      name="body">{{$post->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="category_id">Category</label>
            <select class="custom-select" name="category_id">
            @foreach($categories as $category)
                    <option value="{{$category->id}}" @if($category == $post->category) selected @endif>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="tag_1">Choose tag</label>
            <select class="custom-select" id="tag_3" name="tag_1">
                <option value="">Open this select menu</option>
            @foreach($tags as $tag)
                    <option value="{{$tag->name}}">{{$tag->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="tag_2">Choose tag</label>
            <select class="custom-select" id="tag_3" name="tag_2">
                <option value="">Open this select menu</option>
            @foreach($tags as $tag)
                    <option value="{{$tag->name}}">{{$tag->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="tag_3">Choose tag</label>
            <select class="custom-select" id="tag_3" name="tag_3">
                <option value="">Open this select menu</option>
            @foreach($tags as $tag)
                    <option value="{{$tag->name}}">{{$tag->name}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>
    </div>
@endsection
