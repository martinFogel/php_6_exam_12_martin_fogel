@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="border-bottom pb-3 mb-3">
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 .img-fluid. w-100%  h-auto ">
                <div class="col mb-4">
                    <br>
                    <p>{{$post->body}}</p>
                </div>
                <footer class="blockquote-footer">
                    Author: {{$post->user->name}}, created in
                    <cite title="Created at article">
                        {{$post->created_at->diffForHumans()}}
                    </cite>
                </footer>
            </div>
        </div>
        @if(Auth::check())
            <form method="post" action="{{route('comments.store', ['post' => $post])}}">
                @csrf
                <input type="hidden" id="post_id" value="{{$post->id}}">
                <label for="satisfied"><strong>satisfied:</strong></label>
                <select name="satisfied" id="satisfied">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                <label for="quality"><strong>quality:</strong></label>
                <select name="quality" id="quality">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                <label for="actuality"><strong>actuality:</strong></label>
                <select name="actuality" id="actuality">
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea class="form-control @error('body') is-invalid @enderror" id="body"
                              name="body"></textarea>
                    @error('body')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        @endif
        <div class="row">
            <div class="col-8 scrollit">
                @foreach($post->comments as $comment)
                    <br>
                    @include('comments.comment', ['comment' => $comment])
                @endforeach
            </div>
        </div>
@endsection
