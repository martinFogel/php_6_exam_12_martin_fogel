@extends('layouts.app')

@section('content')
    <div class="container">

        <div style="padding-bottom: 30px;">
            <h3>Posts</h3>
        </div>
        @if(Auth::check())
        <a href="{{route('posts.create')}}" type="button" class="btn btn-outline-primary">
            Create post
        </a>
        @endif
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Body</th>
                <th scope="col">Author</th>
                <th scope="col">Date of creation</th>
                <th scope="col">Date of publication</th>
                <th scope="col">Category</th>
                <th scope="col">Tags</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            @foreach($posts as $post)
                @if($post->publication_date != '')
                    @if($current_date->lt($post->publication_date))
                        <tbody>
                        <tr>
                            <td>{{$post->body}}</td>
                            <td><a href="{{route('users.show', ['user' => $post->user])}}"
                                   class="btn btn-link">{{$post->user->name}}</a></td>
                            <td>{{$post->created_at}}</td>
                            <td>{{$post->publication_date}}</td>
                            <td>{{$post->category->name}}</td>
                            <td>{{$post->tag_1}} {{$post->tag_2}} {{$post->tag_3}}</td>
                            <td><a href="{{route('posts.show', ['post' => $post])}}" class="btn btn-primary">Show</a>
                                @can('update', $post)
                                    <a class="btn btn-outline-primary"
                                       href="{{route('posts.edit', ['post' => $post])}}">
                                        Edit
                                    </a>
                                @endcan
                                @can('update', $post)
                                    <form method="post" action="{{route('posts.destroy', ['post' => $post])}}">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                                    </form>
                                @endcan
                            </td>
                        </tr>
                        </tbody>
                    @endif
                @else
                    @if(auth()->check())
                        @if(auth()->user()->is_admin)
                            <tr>
                                <td>{{$post->body}}</td>
                                <td><a href="{{route('users.show', ['user' => $post->user])}}"
                                       class="btn btn-link">{{$post->user->name}}</a></td>
                                <td>{{$post->created_at}}</td>
                                <td>@if($post->publication_date)
                                        {{$post->publication_date}}
                                    @else
                                        Not published
                                    @endif
                                </td>
                                <td>{{$post->category->name}}</td>
                                <td>{{$post->tag_1}} {{$post->tag_2}} {{$post->tag_3}}</td>
                                <td><a href="{{route('posts.show', ['post' => $post])}}"
                                       class="btn btn-primary">Show</a>
                                    @can('update', $post)
                                        <a class="btn btn-outline-primary"
                                           href="{{route('posts.edit', ['post' => $post])}}">
                                            Edit
                                        </a>
                                    @endcan
                                    @can('update', $post)
                                        <form method="post" action="{{route('posts.destroy', ['post' => $post])}}">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                                        </form>
                                    @endcan
                                    <form method="post" action="{{route('posts.approve', ['post' => $post])}}">
                                        @method('put')
                                        @csrf
                                        <input type="date" name="publication_date" id="publication_date">
                                        <input name="post_id" type="hidden" id="post_id" value="{{$post->id}}">
                                        <button type="submit" class="btn btn-success">Approve</button>
                                    </form>
                                </td>
                            </tr>
                        @endif
                    @endif
                @endif
            @endforeach
        </table>
    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $posts->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
