<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "body" => ['required', 'max:250'],
            "publication_date" => 'nullable|date',
            'tag_1' => 'nullable',
            'tag_2' => 'nullable',
            'tag_3' => 'nullable'
        ];
    }
    public function messages(): array
    {
        return [
            'name.required' => 'Нужно ввести данные в поле: :attribute',
            'name.max' => 'Нужно ввести данные в поле: :attribute',
            'body.required' => 'Нужно ввести данные в поле: :attribute',
            'description.max' => 'Нужно ввести данные в поле: :attribute',
        ];
    }
}
