<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param User $user
     * @return Application|Factory|View
     */
    public function show(User $user)
    {
        $rating_quality = 0;
        $rating_actuality = 0;
        foreach ($user->posts as $post) {
            foreach ($post->comments as $comment) {
                $rating_quality += $comment->quality;
                $rating_actuality += $comment->actuality;
            }
        }
        return view('users.show', compact('user', 'rating_actuality', 'rating_quality'));
    }
}
