<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $posts = Post::paginate(8);
        $current_date = Carbon::now();
        return view('posts.index', compact('posts', 'current_date'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.create', compact('categories', 'tags'));
    }


    /**
     * @param PostRequest $request
     * @return RedirectResponse
     */
    public function store(PostRequest $request): RedirectResponse
    {
        $post = new Post();
        $post->body = $request->input('body');
        $post->category_id = $request->input('category_id');
        $post->user_id = auth()->user()->id;
        $post->tag_1 = $request->input('tag_1');
        $post->tag_2 = $request->input('tag_2');
        $post->tag_3 = $request->input('tag_3');
        $post->save();
        return redirect()->route('posts.index')->with('status', "Post successfully created!");
    }


    /**
     * @param Post $post
     * @return Application|Factory|View
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }


    /**
     * @param Post $post
     * @return Application|Factory|View
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        $categories = Category::all();
        $tags = Tag::all();
        return view('posts.edit', compact('post','categories', 'tags'));
    }


    /**
     * @param Post $post
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Post $post, PostRequest $request): RedirectResponse
    {
        $this->authorize('update', $post);
        $post->body = $request->input('body');
        $post->tag_1 = $request->input('tag_1');
        $post->tag_2 = $request->input('tag_2');
        $post->tag_3 = $request->input('tag_3');
        $post->update();
        return redirect()->route('posts.index')->with('status', "Post successfully created!");
    }

    /**
     * @param Post $post
     * @param Request $request
     * @return RedirectResponse
     */
    public function approve(Post $post, Request $request): RedirectResponse
    {
        $post->publication_date = $request->input('publication_date');
        $post->save();
        return redirect()->route('posts.index')->with('status', "Post successfully created!");
    }
    /**
     * @param Post $post
     * @return RedirectResponse
     */
    public function destroy(Post $post): RedirectResponse
    {
        $this->authorize('delete', $post);
        $post->delete();
        return redirect()->route('posts.index')->with('status', "Post successfully deleted!");
    }
}
