<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * @param Request $request
     * @param Post $post
     * @return RedirectResponse
     */
    public function store(Request $request, Post $post): RedirectResponse
    {
        $request->validate([
            'body' => 'required|min:5',
            'quality' => 'required',
            'actuality' => 'required',
            'satisfied' => 'required',
        ]);
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->quality = $request->input('quality');
        $comment->actuality = $request->input('actuality');
        $comment->satisfied = $request->input('satisfied');
        $comment->post_id = $post->id;
        $comment->save();
        return redirect()->route('posts.show', $comment->post_id);
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->route('posts.show', $comment->post_id);
    }
}
